﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    public class CourseData
    {
        public string? title { get; set; }
        public string? entity_slug { get; set; }
        public string? uni_slug { get; set; }
    }
}
