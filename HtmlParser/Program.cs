﻿using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;
using RestSharp;
using System.Text.Json;
using CsvHelper;
using System.Globalization;


namespace Parser
{
    class Program
    {
        private static void Main(string[] args)
        {
            List<string> courseInfos = new List<string>(); // To CSV

            GetResponseJSON("https://openedu.ru/", 10, courseInfos); //url, size
            WriteCSVFile(courseInfos);

            Console.ReadLine();
        }

        private static async void GetResponseJSON(string websiteUrl, int size, List<string> courseInfos) // Не Async
        {
            string requestUrl = $"https://openedu.ru/catalog/searchjs?=undefined&type=course&size={size}";

            var client = new RestClient(requestUrl);
            var request = new RestRequest();
            var response = client.Get(request);

            if (response.IsSuccessful)
            {
                CourseResponse courses = JsonSerializer.Deserialize<CourseResponse>(response.Content); // JSON

                ParsePage(courses, websiteUrl, size, courseInfos);
            }
        }

        private static async void ParsePage(CourseResponse courses, string websiteUrl, int size, List<string> courseInfos)
        {
            var httpClient = new HttpClient();

            if (courses != null)
            {
                for (int i = 0; i < size; i++)
                {
                    string courseUrl = $"{websiteUrl}course/{courses.data[i].uni_slug}/{courses.data[i].entity_slug}/";
                    string courseId = courses.data[i].entity_slug;
                    string courseTitle = courses.data[i].title;

                    var courseDescription = "";

                    var html = await httpClient.GetStringAsync(courseUrl); // у httpClient нет GetString(), есть только GetStringAsync()
                    var parser = new HtmlParser();

                    IHtmlDocument htmlDocument = parser.ParseDocument(html);

                    if (htmlDocument.GetElementsByClassName("catalog-block-content") != null)
                    {
                        foreach (var element in htmlDocument.GetElementsByClassName("catalog-block-content"))
                        {
                            courseDescription = element.TextContent;
                            break;
                        }
                    }

                    string courseInfoOutput = $"{i + 1}. {courseTitle}\nID: {courseId}\nURL: {courseUrl}\nDESCRIPTION: {courseDescription}";
                    Console.WriteLine(courseInfoOutput);
                    Console.WriteLine("\n\n########################\n\n");

                    courseInfos.Add(courseInfoOutput);
                }
            }
        }

        private static void WriteCSVFile(List<string> courseInfos)
        {
            using (var writer = new StreamWriter("D:/OpenEdu Курсы.csv"))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture, false))
            {

                foreach (var value in courseInfos)
                {
                    csvWriter.WriteField(value);
                }

                writer.Flush();
            }
        }
    }
}